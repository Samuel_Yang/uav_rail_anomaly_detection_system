from typing import Any
from dronekit import *
from multiprocessing import shared_memory
import cv2
import numpy as np
import time

class Drone():
    __connection_string:str = '/dev/ttyUSB0'
    __vehical_mode:str = 'STABILIZE'   # default fly mode

    __float_len = np.dtype(np.float32).itemsize
    __shm_ctr_command = shared_memory.SharedMemory(name='ctr_command', create=False, size=__float_len * 2)
    __shm_ctr_command_data = np.ndarray(shape=(2,), dtype=np.float32, buffer=__shm_ctr_command.buf)

    vehicle = None
    heading:np.float32 = None
    bias:np.float32 = None


    

    def __init__(self, connection_string='/dev/ttyUSB0', baud=57600):
        self.__connection_string = connection_string
        self.__baud = baud
        self.__vehical_mode = 'GUIDE'

    def connect_drone(self):
        self.vehicle = connect(self.__connection_string, baud=self.__baud, wait_ready=True)

    def disconnect_drone(self):
        self.vehicle.close()

    def get_location(self):
        return self.vehicle.location.global_frame
    
    def get_altitude(self):
        return self.vehicle.location.global_relative_frame.alt
    
    def get_velocity(self):
        return self.vehicle.velocity
    
    def get_heading(self):
        return self.vehicle.heading
    
    def set_flight_mode(self,f_mode):
        self.__vehical_mode = f_mode
        self.vehicle.mode = VehicleMode(self.__vehical_mode)

    def set_groundspeed(self,speed):
        if self.vehicle is not None:
            print("groundspeed set to: " % speed)
            self.vehicle.groundspeed = speed

        
    def print_vehicle_info(self) -> Any:
        if self.vehicle is None:
            print("Not connected to a vehicle.")
            return
        # vehicle is an instance of the Vehicle class
        print( "Autopilot Firmware version: %s" % self.vehicle.version)
        print( "Autopilot capabilities (supports ftp): %s" % self.vehicle.capabilities.ftp)
        print( "Global Location: %s" % self.vehicle.location.global_frame)
        print( "Global Location (relative altitude): %s" % self.vehicle.location.global_relative_frame)
        print( "Local Location: %s" % self.vehicle.location.local_frame)    #NED
        print( "Attitude: %s" % self.vehicle.attitude)
        print( "Velocity: %s" % self.vehicle.velocity)
        print( "GPS: %s" % self.vehicle.gps_0)
        print( "Groundspeed: %s" % self.vehicle.groundspeed)
        print( "Airspeed: %s" % self.vehicle.airspeed)
        print( "Gimbal status: %s" % self.vehicle.gimbal)
        print( "Battery: %s" % self.vehicle.battery)
        print( "EKF OK?: %s" % self.vehicle.ekf_ok)
        print( "Last Heartbeat: %s" % self.vehicle.last_heartbeat)
        print( "Rangefinder: %s" % self.vehicle.rangefinder)
        print( "Rangefinder distance: %s" % self.vehicle.rangefinder.distance)
        print( "Rangefinder voltage: %s" % self.vehicle.rangefinder.voltage)
        print( "Heading: %s" % self.vehicle.heading)
        print( "Is Armable?: %s" % self.vehicle.is_armable)
        print( "System status: %s" % self.vehicle.system_status.state)
        print( "Mode: %s" % self.vehicle.mode.name)    # settable
        print( "Armed: %s" % self.vehicle.armed)    # settable

    def arm(self):
        self.vehicle.groundspeed = 3

        print ("Basic pre-arm checks")
        # Don't try to arm until autopilot is ready
        while not self.vehicle.is_armable:
            print (" Waiting for vehicle to initialise...")
            time.sleep(1)

        print ("Arming motors")
        # Copter should arm in GUIDED mode
        self.vehicle.mode    = VehicleMode(self.__vehical_mode)
        self.vehicle.armed   = True

        while not self.vehicle.armed:
            print (" Waiting for arming...")
            time.sleep(1)

        print ("ARMED! Let's take OFF")
        
    def arm_and_takeoff(self,aTargetAltitude:float):
        """
        Arms vehicle and fly to aTargetAltitude.
        """

        print( "Basic pre-arm checks")
        # Don't try to arm until autopilot is ready
        while not self.vehicle.is_armable:
            print( " Waiting for vehicle to initialise...")
            time.sleep(1)

        print( "Arming motors")
        # Copter should arm in GUIDED mode
        self.vehicle.mode    = VehicleMode("GUIDED")
        self.vehicle.armed   = True

        # Confirm vehicle armed before attempting to take off
        while not self.vehicle.armed:
            print( " Waiting for arming...")
            time.sleep(1)

        print( "Taking off!")
        self.vehicle.simple_takeoff(aTargetAltitude) # Take off to target altitude

        # Wait until the vehicle reaches a safe height before processing the goto (otherwise the command
        #  after Vehicle.simple_takeoff will execute immediately).
        while True:
            print( " Altitude: ", self.vehicle.location.global_relative_frame.alt)
            #Break and return from function just below target altitude.
            if self.vehicle.location.global_relative_frame.alt>=aTargetAltitude*0.95:
                print( "Reached target altitude")
                break
            time.sleep(1)

    def get_ctr_command(self):
        self.heading = self.__shm_ctr_command_data[0]
        self.bias = self.__shm_ctr_command_data[1]
        return self.heading, self.bias
    
    def send_ned_velocity(self, velocity_x, velocity_y, velocity_z, duration):
        """
        Move vehicle in direction based on specified velocity vectors.
        """
        msg = self.vehicle.message_factory.set_position_target_local_ned_encode(
            0,       # time_boot_ms (not used)
            0, 0,    # target system, target component
            mavutil.mavlink.MAV_FRAME_LOCAL_NED, # frame
            0b0000111111000111, # type_mask (only speeds enabled)
            0, 0, 0, # x, y, z positions (not used)
            velocity_x, velocity_y, velocity_z, # x, y, z velocity in m/s
            0, 0, 0, # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
            0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)


        # send command to vehicle on 1 Hz cycle
        for x in range(0,duration):
            self.vehicle.send_mavlink(msg)
            time.sleep(1)
    
    def land(self):
        if self.vehicle is not None:
            self.__vehical_mode='LAND'
            print("Setting LAND mode...")
            self.vehicle.mode = VehicleMode(self.__vehical_mode)

    def return_to_launch_location(self):
        #carefull with using this! It wont detect obstacles!
        self.__vehical_mode='RTL'
        self.vehicle.mode = VehicleMode(self.__vehical_mode)

    def send_movement_command_YAW(self, heading):
        speed = 0 
        direction = 1 #direction -1 ccw, 1 cw
        
        #heading 0 to 360 degree. if negative then ccw 
        
        print("Sending YAW movement command with heading: %f" % heading)

        if heading < 0:
            heading = heading*-1
            direction = -1

        #point drone into correct heading 
        msg = self.vehicle.message_factory.command_long_encode(
            0, 0,       
            mavutil.mavlink.MAV_CMD_CONDITION_YAW, 
            0,          
            heading,    
            speed,      #speed deg/s
            direction,  
            1,          #relative offset 1
            0, 0, 0)    

        # send command to vehicle
        self.vehicle.send_mavlink(msg)

    def send_movement_command_XYA(self, velocity_x, velocity_y, altitude):

        #velocity_x positive = forward. negative = backwards
        #velocity_y positive = right. negative = left
        #velocity_z positive = down. negative = up (Yes really!)

        print("Sending XYZ movement command with v_x(forward/backward): %f v_y(right/left): %f " % (velocity_x,velocity_y))

        msg = self.vehicle.message_factory.set_position_target_local_ned_encode(
            0,      
            0, 0,    
            mavutil.mavlink.MAV_FRAME_BODY_NED,  #relative to drone heading pos relative to EKF origin
            0b0000111111100011, #ignore velocity z and other pos arguments
            0, 0, altitude,
            velocity_x, velocity_y, 0, 
            0, 0, 0, 
            0, 0)    

        self.vehicle.send_mavlink(msg)




        


