from collections import deque
from PIL import Image

class ImageQueue:
    def __init__(self, max_size=10):
        self.queue = deque(maxlen=max_size)
        self.max_size = 10
        self.cnt=0
        self.save_path = '../anomaly_save/'
        

    def push(self, image):
        if image.shape == (160, 160, 3):
            if self.cnt<self.max_size:
                self.queue.append(image)
                self.cnt+=1
            else:
                self.queue.popleft()
                self.queue.append(image)
        else:
            raise ValueError("Image dimensions must be (160, 160, 3)")

    def save_images(self, file_name):
        # Convert the NumPy array to a PIL image
        i=0
        while not self.is_empty():
            data = self.pop()
            image = Image.fromarray(data)
            # Save the image to a file
            image.save(f"{file_name}_{i}.png")
            i+=1
        
    def pop(self):
        if self.is_empty():
            return None
        return self.queue.popleft()

    def is_empty(self):
        return len(self.queue) == 0

    def size(self):
        return len(self.queue)

    def get_images(self):
        return list(self.queue)