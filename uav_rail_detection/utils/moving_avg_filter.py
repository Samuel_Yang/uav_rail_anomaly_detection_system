class MovingAverageFilter:
    def __init__(self, step_size=5):
        self.step_size = step_size     
        self.buffer = []

    def set_step_size(self, step_size):
        self.step_size = step_size

    

    def update(self, new_data):
        self.buffer.append(new_data)
        average = new_data
        
        if len(self.buffer) >= self.step_size:
            chunk = self.buffer[-self.step_size:]
            average = sum(chunk) / len(chunk)

            self.buffer.pop(0)

        return average

#     # Example usage:
# data_stream = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
# step_size = 5
# ma_filter = MovingAverageFilter(step_size)

# for data_point in data_stream:
#     data = ma_filter.update(data_point)
#     # print("Current data point:", data_point)
#     # print("Moving average:", filtered_data)
#     print(data)