import os
import socket
import struct
import cv2
import numpy as np
import math


class FrameSegment(object):
    """ 
    Object to break down image frame segment
    if the size of image exceeds maximum datagram size 
    """
    MAX_DGRAM = 1024
    MAX_IMAGE_DGRAM = MAX_DGRAM - 64  # extract 64 bytes in case UDP frame overflows
    FRAME_WIDTH = 160
    FRAME_HEIGHT = 160
    
    def __init__(self, sock, port=8888, addr="0.0.0.0"):
        self.sock = sock
        self.port = port
        self.addr = addr

    def set_sock(self, addr):
        # self.sock=sock
        self.addr=addr

    def send_none(self):
        """
        no send request or no anomy detected
        send none to client
        """
        self.sock.sendto(''.encode(), self.addr)

    def udp_frame(self, img):
        """ 
        Compress image and Break down
        into data segments 
        """
        
        # resized_img = cv2.resize(img, (self.FRAME_WIDTH, self.FRAME_HEIGHT))
        compress_img = cv2.imencode('.jpg', img)[1]
        dat = compress_img.tobytes()

        size = len(dat)
        count = size // self.MAX_DGRAM
        remain_data = size % self.MAX_DGRAM

        array_pos_start = 0
        total_sent = 0
        
        for _ in range(count):
            chunk = dat[total_sent:total_sent + self.MAX_DGRAM]
            self.sock.sendto(chunk, self.addr)
            total_sent += len(chunk)
        chunk = dat[total_sent:total_sent + remain_data]
        self.sock.sendto(chunk, self.addr)

            
        # while count:
        #     array_pos_end = min(size, array_pos_start + self.MAX_DGRAM)
        #     self.sock.sendto(struct.pack("B", count) +
        #                      dat[array_pos_start:array_pos_end],
        #                      self.addr)
        #     array_pos_start = array_pos_end
        #     count -= 1