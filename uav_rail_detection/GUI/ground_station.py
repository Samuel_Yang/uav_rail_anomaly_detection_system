import sys
import threading
import socket
import threading
import cv2

import numpy as np
import threading
import pickle
import time

from PyQt5 import QtCore, QtGui, QtWidgets
from GCS import Ui_GCS

class Application():
    def __init__(self) -> None:
        self.qt_app = QtWidgets.QApplication(sys.argv)
        self.gui = Ui_GCS()
        self.GCS = QtWidgets.QDialog()
        self.gui.setupUi(self.GCS)
        self.gui.Connect_button.clicked.connect(self.connect_to_drone)
        self.gui.open_cam_button.clicked.connect(self.open_cam_handler)
        # self.gui.open_cam_button.setEnabled(False)
        
        self.server_IP = '192.168.50.25'
        self.server_PORT = 8888
        self.connected:bool = False
        self.frame_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.open_cam_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.drone_info_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        
        self.drone_info_thread = None  
        self.frame_thread = None
        self.drone_info = {}

        self.cam_opened = False
        
        self.anomaly_detected = False
        self.BUFFER_SIZE = 1024

        self.location = None

        self.out = None
        self.start_record_trigger = True

        



    def open_cam_handler(self):
        if self.cam_opened == False:
            self.open_cam_socket.send('open'.encode('utf-8'))
            self.cam_opened = True
            self.gui.open_cam_button.setText('Close Camera')
            print('camera opened')
        else:
            self.open_cam_socket.send('close'.encode('utf-8'))
            self.cam_opened = False
            self.gui.open_cam_button.setText('Open Camera')

            # self.gui.video_frame.setText("")
            # self.gui.video_frame.setPixmap(QtGui.QPixmap("UI/icon.png"))
            # self.gui.video_frame.update()
            print('camera close')

    def frame_handler(self):
        
        self.frame_socket.settimeout(3)
        while self.connected:
            try:
                dat = b''
                self.frame_socket.sendto('a'.encode('utf-8'),(self.server_IP,self.server_PORT))
                while True:
                    try:
                        data, _ = self.frame_socket.recvfrom(self.BUFFER_SIZE)  # Adjust buffer size as needed
                        dat+=data
                        if len(data)<self.BUFFER_SIZE:
                            break
                    except socket.timeout:
                        print('frame timeout')
                        dat = b''
                        break
                # print(dat)

                if dat == b'':
                    self.cam_opened = False
                    self.gui.video_frame.setText("")
                    self.gui.video_frame.setPixmap(QtGui.QPixmap("UI/icon.png"))
                    self.gui.video_frame.update()
                else:
                    
                    self.cam_opened = True

                    # print('frame received')
                    frame =  cv2.imdecode(np.frombuffer(dat, dtype=np.uint8), cv2.IMREAD_COLOR)
                    

                    if frame is not None:
                        # height, width, channel = frame.shape
                        
                        if self.out is not None:
                            self.out.write(frame)

                        frame = cv2.resize(frame, (480, 320), cv2.INTER_AREA)    
                        bytes_per_line = 3 * 480
                        q_image = QtGui.QImage(frame.data, 480, 320, bytes_per_line, QtGui.QImage.Format_RGB888)
                        pixmap = QtGui.QPixmap.fromImage(q_image)

                        # Update the QLabel with the received video frame
                        self.gui.video_frame.setPixmap(pixmap)
                        self.gui.video_frame.update()
    
            except Exception as e:
                print(f"Error frame_handler: {str(e)}")
                continue



            

    def drone_info_handler(self):
        self.drone_info_socket.settimeout(3)

        try:
            while self.connected:
                self.drone_info_socket.sendto('a'.encode(), (self.server_IP,self.server_PORT+1))

                try:
                    # Receive the pickled data from the server
                    data_serialized, _ = self.drone_info_socket.recvfrom(self.BUFFER_SIZE)
                except socket.timeout:
                    continue
                if not data_serialized:
                    # Server has closed the connection or no more data to receive
                    print('Server has closed the connection or no more data to receive...')
                    break

                self.drone_info = pickle.loads(data_serialized)
                # print(self.drone_info)
                if self.drone_info['Anomaly']=='Anomaly detected':
                    self.anomaly_detected = True
                    
                else:
                    self.anomaly_detected = False

                self.location = str(self.drone_info['Location'])

                if self.anomaly_detected and self.start_record_trigger:
                    # Define the codec and create a VideoWriter object to save the video.
                    fourcc = cv2.VideoWriter_fourcc(*'XVID')  # Codec for AVI format
                    self.out = cv2.VideoWriter(f'../anomaly_save/{self.drone_info["Location"]}.avi', fourcc, 15, (160, 160))  # Change output file and parameters as needed
                    self.start_record_trigger = False
                elif self.anomaly_detected==False:
                    if self.out is not None:
                        self.out.release()
                    self.out = None
                    self.start_record_trigger = True
                
                
                self.gui.Anomaly.setText(self.drone_info['Anomaly'])
                self.gui.Altitude.setText(self.drone_info['Altitude'])
                self.gui.Location.setText(str(self.drone_info['Location']))
                self.gui.Battery.setText(str(self.drone_info['Battery'])+' V')
                self.gui.Attitude.setText(str(self.drone_info['Attitude']))
                self.gui.VehiclaMode.setText(str(self.drone_info['VehicleMode']))
                
                # time.sleep(0.5)

        except Exception as e:
            print(f'Error at drone_info_handler: {e}')

    def connect_to_drone(self):
        self.server_IP = self.gui.IP.text()
        self.server_PORT = int(self.gui.Port.text())

        if self.connected == False:
            self.connected = True

            self.open_cam_socket.connect((self.server_IP, self.server_PORT+2))
            
            # Enable the open camera button
            self.gui.open_cam_button.setEnabled(True)
            self.drone_info_thread = threading.Thread(target=self.drone_info_handler)
            self.drone_info_thread.daemon = True
            
            self.frame_thread = threading.Thread(target=self.frame_handler)
            self.frame_thread.daemon = True
            
            self.frame_thread.start()
            self.drone_info_thread.start()

            
            self.gui.Connect_button.setText('Disconnect')

        else:
            self.connected = False
            # self.frame_thread.join()
            # self.drone_info_thread.join()
            self.GUI_reset()
            self.gui.Connect_button.setText('Connect')
            self.gui.open_cam_button.setEnabled(False)
        # self.frame_thread.join()
        # self.drone_info_thread.join()



    def GUI_reset(self):
        self.gui.video_frame.setText("")
        self.gui.video_frame.setPixmap(QtGui.QPixmap("UI/icon.png"))
        self.gui.video_frame.update()

        self.gui.Anomaly.setText('-')
        self.gui.Altitude.setText('-')
        self.gui.Location.setText('-')
        self.gui.Battery.setText('-')
        self.gui.Attitude.setText('-')
        self.gui.VehiclaMode.setText('-')
            
            
    def close_window(self):
        self.GCS.close()
        self.open_cam_socket.close()
        self.frame_socket.close()
        self.drone_info_socket.close()
        sys.exit(0)

    def run(self):
        self.GCS.show()
        sys.exit(self.qt_app.exec())



if __name__ == "__main__":
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    application = Application()
    application.run()
    # sys.exit(application.qt_app.exec_())