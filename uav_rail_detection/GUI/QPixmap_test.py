import sys
import cv2
import numpy as np
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QLabel, QVBoxLayout
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt, QTimer, QThread, pyqtSignal
import socket

# Global variables
VIDEO_WIDTH = 640
VIDEO_HEIGHT = 480
UDP_IP = "0.0.0.0"  # Update with the actual IP address of your UDP server
UDP_PORT = 8888      # Update with the actual UDP port

class VideoReceiverThread(QThread):
    frame_received = pyqtSignal(np.ndarray)

    def __init__(self):
        super().__init__()
        self.capture = cv2.VideoCapture(0)  # Use 0 for the default camera, update as needed
        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def run(self):
        while True:
            _, frame = self.capture.read()
            _, frame_encoded = cv2.imencode('.jpg', frame)
            self.udp_socket.sendto(frame_encoded.tobytes(), (UDP_IP, UDP_PORT))
            self.frame_received.emit(frame)

class VideoDisplayWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.video_label = QLabel(self)
        layout = QVBoxLayout()
        layout.addWidget(self.video_label)
        self.setLayout(layout)

    def set_frame(self, frame):
        height, width, channel = frame.shape
        bytes_per_line = 3 * width
        q_img = QImage(frame.data, width, height, bytes_per_line, QImage.Format_RGB888)
        self.video_label.setPixmap(QPixmap.fromImage(q_img))

class VideoReceiverApp(QApplication):
    def __init__(self, argv):
        super().__init__(argv)
        self.receiver_thread = VideoReceiverThread()
        self.main_window = QMainWindow()
        self.video_display = VideoDisplayWidget()
        self.receiver_thread.frame_received.connect(self.video_display.set_frame)
        self.receiver_thread.start()
        self.main_window.setWindowTitle("Video Receiver")
        self.main_window.setGeometry(100, 100, VIDEO_WIDTH, VIDEO_HEIGHT)
        self.main_window.setCentralWidget(self.video_display)
        self.main_window.show()

if __name__ == "__main__":
    app = VideoReceiverApp(sys.argv)
    sys.exit(app.exec_())
